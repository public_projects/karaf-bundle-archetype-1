package org.green.api.impl;

import org.green.api.SampleService;

public class SampleServiceImpl implements SampleService {

    public String getGreeting(final String name) {
        return "hello, " + name;
    }
}
