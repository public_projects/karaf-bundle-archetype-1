package org.green.api;

public interface SampleService {
    String getGreeting(final String name);
}
